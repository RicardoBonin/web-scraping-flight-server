const axios = require('axios')
const cheerio = require('cheerio')

const fetchData = async(url) => {
    const result= await axios.get(url)
    return result.data
}

const main = async () => {
    const content = await fetchData('https://floripa-airport.com/')
    const $ = cheerio.load(content)
    let flights = []

    $('div#partidas').each((i, e) => {
        const destino = $(e).find('.row.justify-content-around.lineTabela > .celulaBox.destino > em').text();
        const voo = $(e).find('.celulaBox.voo > em').text();
        const horaPartida = $(e).find('.celulaBox.horario > em').text();
        
        const data = {destino, voo, horaPartida}
        flights.push(data)
    })
    console.log(flights)
}
main()